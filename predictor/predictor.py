from __future__ import print_function
import os
from collections import defaultdict
from multiprocessing.pool import ThreadPool

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from more_itertools import rstrip
from sklearn import preprocessing
from scipy import stats
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.preprocessing import MinMaxScaler

df = pd.read_csv('average_faces.csv')

df = df[['5_star_count',
         '4_star_count',
         '3_star_count',
         '2_star_count',
         '1_star_count',
         'average_rating',
         'writer_id',
         'review_count',
         'format',
         'amount_of_pages',
         'published',
         'genre_0',
         'amount_of_faces',
         'authorScore',
         'titleScore',
         'joy:',
         'sorrow',
         'anger',
         'surprise',
         'under_exposed',
         'blurred',
         'headwear',
         'face_confidence'
         ]]

le = preprocessing.LabelEncoder()
df['format'] = le.fit_transform(df['format'])
df['genre_0'] = le.fit_transform(df['genre_0'])
df = df.replace('1 page', '1')

df = df.astype(float)
df = df.dropna()
# df = df[df["5_star_count"] != -1]
df = df.replace(-1, 0)
transformations = []
for col in df.columns:
    before = df[col].skew()
    log1p = np.log1p(df[col]).skew()
    sqrt = np.sqrt(df[col]).skew()
    # stats = stats.boxcox(df[col])[0]
    abs_log1p = abs(log1p)
    abs_sqrt = abs(sqrt)
    if abs_sqrt < abs_log1p:
        transformations.append({"transformation": "sqrt", "column": col, "skew": abs_sqrt})
        df[col] = np.sqrt(df[col]).skew()
    else:
        transformations.append({"transformation": "log1p", "column": col, "skew": abs_log1p})
        df[col] = np.log1p(df[col]).skew()

[print(x) for x in transformations]


####### SGD

from sklearn.linear_model import SGDRegressor
from sklearn.model_selection import train_test_split

X = df.loc[:, df.columns != 'average_rating']
y = df['average_rating']

# X_train, X_test, y_train, y_test = train_test_split(
#     X, y, test_size=0.33, random_state=42)
#
# min_max_scaler = MinMaxScaler()
# X_train_scaled = min_max_scaler.fit_transform(X_train, y_train)
# min_max_scaler = MinMaxScaler()
# X_test_scaled = min_max_scaler.fit_transform(X_test, y_test)

# Split the data into two parts with 1000 points in the test data
# This creates a generator
strat_shuff_split = StratifiedShuffleSplit(n_splits=1, test_size=0.33,  random_state=42)

# Get the index values from the generator
train_idx, test_idx = next(strat_shuff_split.split(data[feature_cols], data['species']))

# Create the data sets
X_train = data.loc[train_idx, feature_cols]
y_train = data.loc[train_idx, 'species']

X_test = data.loc[test_idx, feature_cols]
y_test = data.loc[test_idx, 'species']


reg = SGDRegressor(max_iter=1000, penalty='elasticnet')
reg.fit(X_train_scaled, y_train)
print(reg.score(X_test_scaled, y_test))


# df['format'] =

# data_path = ['../data']
# filepathfaces = os.sep.join(data_path + ['faces.csv'])
# filepathcolors = os.sep.join(data_path + ['colors.csv'])
# filepathfull_parse = os.sep.join(data_path + ['full_parse.csv'])
# filepathobjects = os.sep.join(data_path + ['objects.csv'])
#
# facesdata = pd.read_csv(filepathfaces)
# # colorsdata = pd.read_csv(filepathcolors)
# data = pd.read_csv(filepathfull_parse)
# # objectdata = pd.read_csv(filepathobjects)
# final_dataframe = pd.DataFrame()
#
# data = data[(data['average_rating'] > 0) & (data['published'] <= 2021) ]
# genres = data["genre_0"].value_counts()
# genres = genres[(genres > 50)]
#
# replace_dict = {"UNKNOWN": 0,
#                 "VERY_UNLIKELY": 1,
#                 "UNLIKELY": 2,
#                 "POSSIBLE": 3,
#                 "LIKELY": 4,
#                 "VERY_LIKELY": 5,
#                 }
#
# facesdata = facesdata.replace(replace_dict)
#
# newbooks = pd.DataFrame()
# #calculate average face expression
# facedata = ['joy:', 'sorrow', 'anger', 'surprise', 'under_exposed','blurred', 'headwear', 'face_confidence']
# for bookindex, bookrow in data.iterrows():
#     bookrow = bookrow.append(pd.Series(0, index=facedata))
#     isbn = bookrow['isbn']
#     faces = facesdata[facesdata['isbn'] == isbn]
#     numfaces = 0
#     for faceindex, facerow in faces.iterrows():
#         numfaces += 1
#         bookrow[facedata] += facerow[facedata]
#
#     #get average
#     if(numfaces > 0):
#         bookrow[facedata] = (bookrow[facedata] / numfaces)
#
#     #add that wierd array thingy at the end to make sure the column names don't shuffle
#     newbooks = newbooks.append(bookrow, ignore_index=True)[bookrow.index.tolist()]
#     if bookindex % 100 == 0:
#         print("parsed book {}".format(bookindex))
#
# # data_list = [x for x in data.iterrows()]
# # for x in data.iterrows():
# #     print(x)
# #     print(type(x))
# #     break
# #
# #
# # def test(tup):
# #     bookindex = tup[0]
# #
# #     bookrow = tup[1]
# #     bookrow = bookrow.append(pd.Series(0, index=facedata))
# #     isbn = bookrow['isbn']
# #     faces = facesdata[facesdata['isbn'] == isbn]
# #     numfaces = 0
# #     for faceindex, facerow in faces.iterrows():
# #         numfaces += 1
# #         bookrow[facedata] += facerow[facedata]
# #
# #     # get average
# #     if (numfaces > 0):
# #         bookrow[facedata] = (bookrow[facedata] / numfaces)
# #
# #     # add that wierd array thingy at the end to make sure the column names don't shuffle
# #     global newbooks
# #     newbooks = newbooks.append(bookrow, ignore_index=True)[bookrow.index.tolist()]
# #     if bookindex % 100 == 0:
# #         print("parsed book {}".format(bookindex))
# #
# # pool = ThreadPool(100)
# # results = pool.map(test, data_list)
# #
# # newbooks = newbooks.replace(r'\\n',' ', regex=True)
#
# newbooks = newbooks.replace('\n',' ', regex=True)
#
# print("Done parsing faces")
# newbooks.to_csv("average_faces.csv")

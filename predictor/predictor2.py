from __future__ import print_function
import os
from collections import defaultdict
from multiprocessing.pool import ThreadPool

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import preprocessing

from sklearn.model_selection import StratifiedShuffleSplit


data_path = ['../data']
filepathfaces = os.sep.join(data_path + ['faces.csv'])
filepathcolors = os.sep.join(data_path + ['colors.csv'])
filepathfull_parse = os.sep.join(data_path + ['full_parse.csv'])
filepathobjects = os.sep.join(data_path + ['objects.csv'])

facesdata = pd.read_csv(filepathfaces)
# colorsdata = pd.read_csv(filepathcolors)
data = pd.read_csv(filepathfull_parse)
# objectdata = pd.read_csv(filepathobjects)
final_dataframe = pd.DataFrame()

# data = data[data['isbn'] == 9781785941689]
data = data[(data['average_rating'] > 0) & (data['published'] <= 2021)]
genres = data["genre_0"].value_counts()
genres = genres[(genres > 50)]

replace_dict = {"UNKNOWN": 0,
                "VERY_UNLIKELY": 1,
                "UNLIKELY": 2,
                "POSSIBLE": 3,
                "LIKELY": 4,
                "VERY_LIKELY": 5,
                }

facesdata = facesdata.replace(replace_dict)

newbooks = pd.DataFrame()
#calculate average face expression
facedata = ['joy:', 'sorrow', 'anger', 'surprise', 'under_exposed','blurred', 'headwear', 'face_confidence']
# for bookindex, bookrow in data.iterrows():
#     bookrow = bookrow.append(pd.Series(0, index=facedata))
#     isbn = bookrow['isbn']
#     faces = facesdata[facesdata['isbn'] == isbn]
#     numfaces = 0
#     for faceindex, facerow in faces.iterrows():
#         numfaces += 1
#         bookrow[facedata] += facerow[facedata]
#
#     #get average
#     if(numfaces > 0):
#         bookrow[facedata] = (bookrow[facedata] / numfaces)
#
#     #add that wierd array thingy at the end to make sure the column names don't shuffle
#     newbooks = newbooks.append(bookrow, ignore_index=True)[bookrow.index.tolist()]
#     if bookindex % 100 == 0:
#         print("parsed book {}".format(bookindex))

data_list = [x for x in data.iterrows()]
for x in data.iterrows():
    print(x)
    print(type(x))
    break


def test(tup):
    bookindex = tup[0]

    bookrow = tup[1]
    bookrow = bookrow.append(pd.Series(0, index=facedata))
    isbn = bookrow['isbn']
    faces = facesdata[facesdata['isbn'] == isbn]
    numfaces = 0
    for faceindex, facerow in faces.iterrows():
        numfaces += 1
        bookrow[facedata] += facerow[facedata]

    # get average
    if (numfaces > 0):
        bookrow[facedata] = (bookrow[facedata] / numfaces)

    # add that wierd array thingy at the end to make sure the column names don't shuffle
    global newbooks
    newbooks = newbooks.append(bookrow, ignore_index=True)[bookrow.index.tolist()]
    if bookindex % 100 == 0:
        print("parsed book {}".format(bookindex))

pool = ThreadPool(8)
results = pool.map(test, data_list)

newbooks = newbooks.replace('\n',' ', regex=True)
print("Done parsing faces")
newbooks.to_csv("average_faces.csv")
from __future__ import print_function
import os
from collections import defaultdict

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import preprocessing

from sklearn.model_selection import StratifiedShuffleSplit

data_path = ['data']
filepathfaces = os.sep.join(data_path + ['faces.csv'])
filepathcolors = os.sep.join(data_path + ['colors.csv'])
filepathfull_parse = os.sep.join(data_path + ['full_parse.csv'])
filepathobjects = os.sep.join(data_path + ['objects.csv'])

facesdata = pd.read_csv(filepathfaces)
colorsdata = pd.read_csv(filepathcolors)
data = pd.read_csv(filepathfull_parse)
# objectdata = pd.read_csv(filepathobjects)

data = data[(data['average_rating'] > 0) & (data['published'] <= 2021)]
genres = data["genre_0"].value_counts()
genres = genres[(genres > 100)]

print(genres)

final_dataframe = pd.DataFrame()

for genre in genres.index:
    full_parsedata = data[data["genre_0"] == genre]

    # merged1 = full_parsedata.merge(facesdata, on=['isbn'], how='outer')
    # merged = merged1.merge(colorsdata, on=['isbn'], how='outer')

    # dataset = merged[[
    #     'average_rating', 'review_count',
    #     'amount_of_pages', 'published', 'first_published', 'amount_of_faces',
    #     'amount_of_objects', 'authorScore', 'titleScore', 'joy:', 'sorrow', 'anger', 'surprise', 'under_exposed',
    #     'blurred', 'headwear', 'face_confidence', 'red', 'green',
    #     'blue', 'score', 'pixel_fraction'
    # ]]

    dataset = full_parsedata[[
        'average_rating', 'review_count',
        'amount_of_pages', 'published', 'first_published', 'amount_of_faces',
        'amount_of_objects', 'authorScore', 'titleScore'
    ]]

    replace_dict = {"UNKNOWN": 0,
                              "VERY_UNLIKELY": 1,
                              "UNLIKELY": 2,
                              "POSSIBLE": 3,
                              "LIKELY": 4,
                              "VERY_LIKELY": 5,
                              }

    X_data = dataset.replace(replace_dict)
    X_data = X_data.fillna(0)

    X_data['review_count'] = pd.to_numeric(X_data['review_count'], errors='coerce')
    X_data['amount_of_pages'] = pd.to_numeric(X_data['amount_of_pages'], errors='coerce')
    x = X_data.astype(float)
    x = x.fillna(0)



    def generatePlots(x, heatmap_name, pairplot_name):

        final_dataframe[genre + "_rating"] = x.corr()['average_rating'][1:]
        print("added {}".format(genre))
        #
        # plt.subplots(figsize=(200, 200))
        # graph = sns.pairplot(x, corner=True)
        # # graph.map_lower(sns.kdeplot, levels=4, color=".2")
        # graph.savefig("{}.png".format(pairplot_name), format="png")
        # print("done building pairplot {}".format(pairplot_name))

        # plt.subplots(figsize=(4, 20))
        # plt.yticks(rotation=0)
        # plt.tick_params(axis='both', which='major', labelbottom = True, bottom=True, top = True, labeltop=True)
        # graph = sns.heatmap(ratings, annot=True, fmt='.1g', vmin=-1, vmax=1, cmap="YlGnBu", square=True)
        # print("done building heatmap {}".format(heatmap_name))
        # graph.figure.savefig("{}.png".format(heatmap_name))

        # plt.subplots(figsize=(200, 200))
        # graph = sns.pairplot(x, corner=True)
        # # graph.map_lower(sns.kdeplot, levels=4, color=".2")
        # graph.savefig("{}.png".format(pairplot_name), format="png")
        # print("done building pairplot {}".format(pairplot_name))

    generatePlots(x, "./imgs/heatmap_{}".format(genre), "./imgs/pairplot_{}".format(genre))

plt.subplots(figsize=(30, 30))
plt.yticks(rotation=0)
plt.tick_params(axis='both', which='major', labelbottom = True, bottom=True, top = True, labeltop=True)
graph = sns.heatmap(final_dataframe, annot=True, fmt='.1g', vmin=-1, vmax=1, cmap="YlGnBu", square=True)
# print("done building heatmap {}".format(heatmap_name))
graph.figure.savefig("{}.png".format("lmao2"))
from __future__ import print_function
import os
from collections import defaultdict

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import preprocessing

from sklearn.model_selection import StratifiedShuffleSplit

data_path = ['data']
filepathfaces = os.sep.join(data_path + ['faces.csv'])
filepathcolors = os.sep.join(data_path + ['colors.csv'])
filepathfull_parse = os.sep.join(data_path + ['full_parse.csv'])
filepathobjects = os.sep.join(data_path + ['objects.csv'])

facesdata = pd.read_csv(filepathfaces)
colorsdata = pd.read_csv(filepathcolors)
full_parsedata = pd.read_csv(filepathfull_parse)
# objectdata = pd.read_csv(filepathobjects)

# print(full_parsedata["genre_0"].value_counts())
full_parsedata = full_parsedata[full_parsedata["genre_0"] == "nonfiction"]

merged1 = full_parsedata.merge(facesdata, on=['isbn'], how='outer')
merged = merged1.merge(colorsdata, on=['isbn'], how='outer')

merged = merged[(merged["face_confidence"] > 0.8) & (merged["average_rating"] > 0)]


# merged = merged2.merge(objectdata, on=['isbn'], how='outer')
# print(merged.loc[merged["amount_of_faces"] > 0]["hd_cover_link_1"].unique())
# exit(0)

# dataset = merged[[
#     'average_rating', '5_star_count', '4_star_count', '3_star_count',
#     '2_star_count', '1_star_count', 'review_count',
#     'amount_of_pages', 'published', 'first_published', 'amount_of_faces',
#     'amount_of_objects', 'authorScore', 'titleScore', 'joy:', 'sorrow', 'anger', 'surprise', 'under_exposed',
#     'blurred', 'headwear', 'face_confidence', 'red', 'green',
#     'blue', 'score', 'pixel_fraction'
# ]]

dataset = merged[[
    'average_rating', 'review_count',
    'amount_of_pages', 'published', 'first_published', 'amount_of_faces',
    'amount_of_objects', 'authorScore', 'titleScore', 'joy:', 'sorrow', 'anger', 'surprise', 'under_exposed',
    'blurred', 'headwear', 'face_confidence', 'red', 'green',
    'blue', 'score', 'pixel_fraction'
]]

replace_dict = {"UNKNOWN": 0,
                          "VERY_UNLIKELY": 1,
                          "UNLIKELY": 2,
                          "POSSIBLE": 3,
                          "LIKELY": 4,
                          "VERY_LIKELY": 5,
                          }

X_data = dataset.replace(replace_dict)
# y_data = dataset['average_rating']
# X_data = dataset.loc[:, dataset.columns != 'average_rating']
X_data = X_data.fillna(0)

# replace_dict = {"None": 0}
#
# X_data = X_data.replace({
#     'joy:': replace_dict,
#     'sorrow': replace_dict,
#     'anger': replace_dict,
#     'surprise': replace_dict,
#     'under_exposed': replace_dict,
#     'blurred': replace_dict,
#     'headwear': replace_dict,
#     'face_confidence': replace_dict,
# })

# X_data[['joy:', 'sorrow', 'anger', 'surprise', 'under_exposed',
#         'blurred', 'headwear']] = X_data[['joy:', 'sorrow', 'anger', 'surprise', 'under_exposed',
#                                           'blurred', 'headwear']].replace({
#     "None": 0
# })
# categorialcols = [
#     'genre_0', 'joy:', 'sorrow', 'anger', 'surprise', 'under_exposed',
#     'blurred', 'headwear'
# ]
#
# x = pd.get_dummies(dataset, columns=categorialcols)
# for i in x.columns:
#     print(i)


# X_data['5_star_count'] = pd.to_numeric(X_data['5_star_count'], errors='coerce')
# X_data['4_star_count'] = pd.to_numeric(X_data['4_star_count'], errors='coerce')
# X_data['3_star_count'] = pd.to_numeric(X_data['3_star_count'], errors='coerce')
# X_data['2_star_count'] = pd.to_numeric(X_data['2_star_count'], errors='coerce')
# X_data['1_star_count'] = pd.to_numeric(X_data['1_star_count'], errors='coerce')
X_data['review_count'] = pd.to_numeric(X_data['review_count'], errors='coerce')
X_data['amount_of_pages'] = pd.to_numeric(X_data['amount_of_pages'], errors='coerce')
x = X_data.astype(float)
x = x.fillna(0)

# x.to_csv("onehotencoded.csv")
# print(x.columnsX_data
# x.to_csv("dikkedoei.csv")
# corr = np.corrcoef(x.values, rowvar=False)
# s = corr.unstack()
# so = s.sort_values(kind="quicksort", ascending=False)
# so.to_csv("corr.csv")
# print("done calc corr")

def generatePlots(x, heatmap_name, pairplot_name):
    plt.subplots(figsize=(50, 50))
    plt.tick_params(axis='both', which='major', labelbottom = True, bottom=True, top = True, labeltop=True)
    graph = sns.heatmap(x.corr(), annot=True, fmt='.1g', vmin=-1, vmax=1, cmap="YlGnBu")
    print("done building heatmap {}".format(heatmap_name))
    graph.figure.savefig("{}.svg".format(heatmap_name), format="svg")

    plt.subplots(figsize=(200, 200))
    graph = sns.pairplot(x, corner=True)
    graph.map_lower(sns.kdeplot, levels=4, color=".2")
    graph.savefig("{}.png".format(pairplot_name), format="png")
    print("done building pairplot {}".format(pairplot_name))

c = x.corr()
generatePlots(x, "heatmap_1", "pairplot_1")
s = c.unstack()
so = s.sort_values(kind="quicksort", ascending=False)
so.to_csv("corr.csv")
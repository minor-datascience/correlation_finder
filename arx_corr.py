from __future__ import print_function
import os
from collections import defaultdict
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.model_selection import StratifiedShuffleSplit

df = pd.read_csv("./adult_modified.csv", delimiter=';')

df = df[~(df == '?').any(axis=1)]
print("Amount of incomplete values: " + str(round((1 - len(df) / len(df)) * 100, 2)) + "%")

columns = ['salary']
for column in columns:
    df[column] = df[column].astype('category').cat.codes

df = df[['age', 'fnlwgt', 'education-num', 'capital-gain', 'capital-loss', 'hours-per-week', 'salary']]

corr = df.corr()
# Getting the Upper Triangle of the co-relation matrix
matrix = np.triu(corr)

plt.subplots(figsize=(10, 10))
plt.yticks(rotation=0)
# plt.tick_params(axis='both', which='major', labelbottom = True, bottom=True, top = True, labeltop=True)
graph = sns.heatmap(corr, mask=matrix, annot=True, fmt='.1g', vmin=-1, vmax=1, cmap="YlGnBu", square=True)
print("done building heatmap {}".format("lmao3.png"))
graph.figure.savefig("{}.png".format("lmao3"))

plt.clf()
plt.subplots(figsize=(200, 200))
graph = sns.pairplot(df, corner=True, hue="salary")
graph.savefig("{}.png".format("jaa"), format="png")
print("done building pairplot {}".format("jaa"))